Wattsim Flask
===============

Après avoir suivi les instructions d'installation du Backend (https://gitlab.com/AdrienPa/backend) pour créer la base de données, suivre les étapes suivantes :

Prérequis
---------

* Python 3.6 ou ultérieur
* Un clone git du projet ('git clone https://gitlab.com/AdrienPa/wattsim_flask.git')
* Pipenv

Installation
------------

* Dans la console, entrer la commande "pipenv install" pour installer les modules requis.
* Remplir le fichier database.ini avec les variables d'environnement dont vous avez besoin pour se connecter à la base de données (ici : host, database, user, password, port).
* Modifier les valeurs du fichier .env de la même façon.

Exécution
-----------

* Rentrer dans le shell ('pipenv shell') puis lancer le serveur Flask ('flask run').