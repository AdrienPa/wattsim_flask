import base64
from io import BytesIO
import matplotlib.pyplot as plt
import numpy as np
import psycopg2
from psycopg2 import Error
from config import config


def CreaPng():
    with BytesIO() as buf:
        plt.savefig(buf, format="png", bbox_inches='tight')
        NomPngTampon = base64.b64encode(buf.getbuffer()).decode("ascii")
        return NomPngTampon


params = config()

def Production_totale():
    conn = psycopg2.connect(**params)
    cur = conn.cursor()
    tot_hydro = '''SELECT CAST(SUM(hydro) AS INTEGER) from APP_MEASUREMENT;'''
    tot_solar = '''SELECT CAST(SUM(solar) AS INTEGER) from APP_MEASUREMENT;'''
    tot_wind = '''SELECT CAST(SUM(wind) AS INTEGER) from APP_MEASUREMENT;'''
    cur.execute(tot_hydro)
    th = cur.fetchone()
    cur.execute(tot_solar)
    ts = cur.fetchone()
    cur.execute(tot_wind)
    tw = cur.fetchone()

    labels = "Hydro", "Solaire", "Eolien"
    sizes = {"Hydro": th[0], "Solaire": ts[0], "Vent": tw[0]}
    data = list(sizes.values())

    def make_autopct(data):
        def my_autopct(pct):
            total = sum(data)
            val = int(round(pct*total/100.0))
            return '{p:.2f}%  ({v:d})'.format(p=pct,v=val)
        return my_autopct
    

    fig1, ax1 = plt.subplots()
    ax1.pie(data, labels=labels, autopct=make_autopct(data)) #autopct='%1.1f%%')
    ax1.axis("equal")
    ax1.set_title("Production totale pour chaque catégorie en mW", size=15, weight="bold")

    return CreaPng()


def Production_moyenne():
    conn = psycopg2.connect(**params)
    cur = conn.cursor()
    moy_hydro = '''SELECT CAST(AVG(hydro) AS DECIMAL (10,2)) from APP_MEASUREMENT;'''
    moy_solar = '''SELECT CAST(AVG(solar) AS DECIMAL (10,2)) from APP_MEASUREMENT;'''
    moy_wind = '''SELECT CAST(AVG(wind) AS DECIMAL (10,2)) from APP_MEASUREMENT;'''
    cur.execute(moy_hydro)
    mh = cur.fetchone()
    cur.execute(moy_solar)
    ms = cur.fetchone()
    cur.execute(moy_wind)
    mw = cur.fetchone()


    objects = ("Hydro", "Solaire", "Eolien")
    y_pos = np.arange(len(objects))

    moy = [mh[0], ms[0], mw[0]]

    plt.subplots(figsize=(10, 6))
    plt.bar(y_pos, moy, align='center')
    plt.xticks(y_pos, objects, fontsize = 12)
    plt.ylabel("Production en mW", fontsize = 12)
    plt.title("Production moyenne pour chaque catégorie", fontsize = 14, fontweight="bold")

    return CreaPng()