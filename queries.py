import psycopg2
from psycopg2 import Error
from config import config


def Connexion_DB():
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
    except (Exception, psycopg2.Error) as error:
        print("Erreur de connexion, vérifiez les paramètres", error)
    return conn


def Total_Production():
    conn = Connexion_DB()
    cur = conn.cursor()
    tot_hydro = '''SELECT CAST(SUM(hydro) AS INTEGER) from APP_MEASUREMENT;'''
    tot_solar = '''SELECT CAST(SUM(solar) AS INTEGER) from APP_MEASUREMENT;'''
    tot_wind = '''SELECT CAST(SUM(wind) AS INTEGER) from APP_MEASUREMENT;'''
    cur.execute(tot_hydro)
    th = cur.fetchone()
    cur.execute(tot_solar)
    ts = cur.fetchone()
    cur.execute(tot_wind)
    tw = cur.fetchone()
    total = sum([th[0], ts[0], tw[0]])

    return f"Vous avez produit au total {th[0]}mW en hydro, {ts[0]}mW en solaire et {tw[0]}mW en vent. Et {total}mw en tout !"


def Moyenne_Production():
    conn = Connexion_DB()
    cur = conn.cursor()
    moy_hydro = '''SELECT CAST(AVG(hydro) AS DECIMAL (10,2)) from APP_MEASUREMENT;'''
    moy_solar = '''SELECT CAST(AVG(solar) AS DECIMAL (10,2)) from APP_MEASUREMENT;'''
    moy_wind = '''SELECT CAST(AVG(wind) AS DECIMAL (10,2)) from APP_MEASUREMENT;'''
    cur.execute(moy_hydro)
    mh = cur.fetchone()
    cur.execute(moy_solar)
    ms = cur.fetchone()
    cur.execute(moy_wind)
    mw = cur.fetchone()
    
    return f"Vous avez produit en moyenne {mh[0]}mW en hydro, {ms[0]}mW en solaire et {mw[0]}mW en vent."


def Deconnexion():
    conn = Connexion_DB()
    cur = conn.cursor()
    cur.close()
    conn.close()
    #print("PostgreSQL connection is closed")


Total_Production()
Moyenne_Production()
Deconnexion()