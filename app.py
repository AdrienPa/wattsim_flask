from flask import Flask, render_template
import graph, queries
from dotenv import load_dotenv

load_dotenv()

app = Flask(__name__)

queries.Connexion_DB()

@app.route("/", methods=["GET"])
def get_graph():
    return render_template(
        "graph.html",
        titre = "Visualisation des statistiques",
        TotalProd = queries.Total_Production(),
        MoyProd = queries.Moyenne_Production(),
        GraphA = graph.Production_totale(),
        GraphB = graph.Production_moyenne()
        )